package com.fuxing.minapp.service;

import com.fuxing.minapp.pojo.SysUser;

public interface SysUserService {

    SysUser selectSysUserByUserName(String username);

    void registerSysUser(SysUser sysUser);

    void modifyPassword(SysUser sysUser) throws Exception;
}
