package com.fuxing.minapp.service.impl;

import com.fuxing.minapp.mapper.SysUserMapper;
import com.fuxing.minapp.pojo.SysUser;
import com.fuxing.minapp.service.SysUserService;
import com.fuxing.minapp.utils.MD5Utils;
import org.n3r.idworker.Sid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper mapper;

    @Autowired
    private Sid sid;

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public SysUser selectSysUserByUserName(String username) {
        SysUser sysUser = mapper.selectSysUserByUserName(username);
        return sysUser;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void registerSysUser(SysUser sysUser){
        try {
            sysUser.setSysUserId(sid.nextShort());
            sysUser.setPassword(MD5Utils.getMD5Str(sysUser.getPassword()));
            mapper.registerSysUser(sysUser);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void modifyPassword(SysUser sysUser) throws Exception {
        sysUser.setPassword(MD5Utils.getMD5Str(sysUser.getPassword()));
        mapper.updatePassword(sysUser);
    }


}
