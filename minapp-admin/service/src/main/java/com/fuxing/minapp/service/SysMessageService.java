package com.fuxing.minapp.service;

import com.fuxing.minapp.pojo.SysMessage;
import com.fuxing.minapp.utils.PagedResult;

public interface SysMessageService {
    void pushMessage(SysMessage sysMessage);

    void delSysMessage(String id);

    PagedResult querySysMessageList(Integer page, int pageSize);
}
