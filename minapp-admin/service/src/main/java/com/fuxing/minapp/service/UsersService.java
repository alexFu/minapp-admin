package com.fuxing.minapp.service;

import com.fuxing.minapp.pojo.Users;
import com.fuxing.minapp.utils.PagedResult;

public interface UsersService {

	public PagedResult queryUsers(Users user, Integer page, Integer pageSize);
	
}
