package com.fuxing.minapp.service.impl;

import com.fuxing.minapp.mapper.SysMessageMapper;
import com.fuxing.minapp.mapper.SysUserMapper;
import com.fuxing.minapp.mapper.UsersMapper;
import com.fuxing.minapp.pojo.*;
import com.fuxing.minapp.service.SysMessageService;
import com.fuxing.minapp.service.UsersService;
import com.fuxing.minapp.utils.DateUtils;
import com.fuxing.minapp.utils.PagedResult;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.n3r.idworker.Sid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class SysMessageServiceImpl implements SysMessageService {

	@Autowired
	private Sid sid;

	@Autowired
	private SysMessageMapper mapper;

	@Autowired
	private SysUserMapper sysUserMapper;

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void pushMessage(SysMessage sysMessage) {
		sysMessage.setId(sid.nextShort());
		sysMessage.setCreateTime(new Date());
		mapper.saveMessage(sysMessage);
	}

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void delSysMessage(String id) {
        mapper.deleteByPrimaryKey(id);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public PagedResult querySysMessageList(Integer page, int pageSize) {
        PageHelper.startPage(page, pageSize);

        SysMessage example = new SysMessage();
        List<SysMessage> list = mapper.showSysMessage();
        list.forEach(obj -> {
            SysUser sysUser = sysUserMapper.selectByPrimaryKey(obj.getSysUserId());
            obj.setSysNickname(sysUser.getNickname());
            obj.setCreateTimeStr(DateUtils.formatDate(obj.getCreateTime(),"yyyy-MM-dd HH:mm:ss"));
        });
        PageInfo<SysMessage> pageList = new PageInfo<>(list);

        PagedResult result = new PagedResult();
        result.setTotal(pageList.getPages());
        result.setRows(list);
        result.setPage(page);
        result.setRecords(pageList.getTotal());

        return result;
    }
}
