package com.fuxing.minapp.service;

import com.fuxing.minapp.pojo.Bgm;
import com.fuxing.minapp.utils.PagedResult;

public interface VideoService {

	public void addBgm(Bgm bgm);
	
	public PagedResult queryBgmList(Integer page, Integer pageSize);
	
	public void deleteBgm(String id);

	public void deleteVideo(String id);
	
	public PagedResult queryReportList(Integer page, Integer pageSize);
	
	public void updateVideoStatus(String videoId, Integer status);

	public PagedResult queryVideoList(Integer page, Integer pageSize);

	PagedResult queryUnReportList(Integer page, Integer pageSize);
}
