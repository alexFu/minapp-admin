package com.fuxing.minapp.service.impl;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fuxing.minapp.enums.BGMOperatorTypeEnum;
import com.fuxing.minapp.mapper.UsersMapper;
import com.fuxing.minapp.pojo.Bgm;
import com.fuxing.minapp.pojo.BgmExample;
import com.fuxing.minapp.pojo.Users;
import com.fuxing.minapp.pojo.Videos;
import com.fuxing.minapp.pojo.vo.Reports;
import com.fuxing.minapp.utils.PagedResult;
import org.n3r.idworker.Sid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.fuxing.minapp.mapper.BgmMapper;
import com.fuxing.minapp.mapper.UsersReportMapperCustom;
import com.fuxing.minapp.mapper.VideosMapper;
import com.fuxing.minapp.service.VideoService;
import com.fuxing.minapp.utils.JsonUtils;
import com.fuxing.minapp.web.util.ZKCurator;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VideoServiceImpl implements VideoService {

	@Autowired
	private VideosMapper videosMapper;
		
	@Autowired
	private BgmMapper bgmMapper;

	@Autowired
	private UsersMapper usersMapper;
	
	@Autowired
	private Sid sid;
	
	@Autowired
	private ZKCurator zkCurator;
	
	@Autowired
	private UsersReportMapperCustom usersReportMapperCustom;

	@Transactional(propagation = Propagation.SUPPORTS)
	@Override
	public PagedResult queryReportList(Integer page, Integer pageSize) {

		PageHelper.startPage(page, pageSize);

		List<Reports> reportsList = usersReportMapperCustom.selectAllVideoReport();

		PageInfo<Reports> pageList = new PageInfo<Reports>(reportsList);

		PagedResult grid = new PagedResult();
		grid.setTotal(pageList.getPages());
		grid.setRows(reportsList);
		grid.setPage(page);
		grid.setRecords(pageList.getTotal());

		return grid;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	@Override
	public PagedResult queryUnReportList(Integer page, Integer pageSize) {

		PageHelper.startPage(page, pageSize);

		List<Reports> reportsList = usersReportMapperCustom.selectAllForbidReport();

		PageInfo<Reports> pageList = new PageInfo<Reports>(reportsList);

		PagedResult grid = new PagedResult();
		grid.setTotal(pageList.getPages());
		grid.setRows(reportsList);
		grid.setPage(page);
		grid.setRecords(pageList.getTotal());

		return grid;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void updateVideoStatus(String videoId, Integer status) {
		
		Videos video = new Videos();
		video.setId(videoId);
		video.setStatus(status);
		videosMapper.updateByPrimaryKeySelective(video);
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	@Override
	public PagedResult queryBgmList(Integer page, Integer pageSize) {
		
		PageHelper.startPage(page, pageSize);
		
		BgmExample example = new BgmExample();
		List<Bgm> list = bgmMapper.selectByExample(example);
		
		PageInfo<Bgm> pageList = new PageInfo<>(list);
		
		PagedResult result = new PagedResult();
		result.setTotal(pageList.getPages());
		result.setRows(list);
		result.setPage(page);
		result.setRecords(pageList.getTotal());
		
		return result;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void addBgm(Bgm bgm) {
		String bgmId = sid.nextShort();
		bgm.setId(bgmId);
		bgmMapper.insert(bgm);
		
		Map<String, String> map = new HashMap<>();
		map.put("operType", BGMOperatorTypeEnum.ADD.type);
		map.put("path", bgm.getPath());
		
		zkCurator.sendBgmOperator(bgmId, JsonUtils.objectToJson(map));
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void deleteBgm(String id) {
		Bgm bgm = bgmMapper.selectByPrimaryKey(id);
		String basePath = "D:"+ File.separator + "workspace" + File.separator + "fileUpload";
		String filePath = basePath + bgm.getPath();
		File file = new File(filePath);
		file.delete();
		bgmMapper.deleteByPrimaryKey(id);
		
		Map<String, String> map = new HashMap<>();
		map.put("operType", BGMOperatorTypeEnum.DELETE.type);
		map.put("path", bgm.getPath());
		
		zkCurator.sendBgmOperator(id, JsonUtils.objectToJson(map));
		
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void deleteVideo(String id) {
		Videos videos = videosMapper.selectByPrimaryKey(id);
		String basePath = "D:"+ File.separator + "workspace" + File.separator + "fileUpload";
		String videoFilePath = basePath + videos.getVideoPath();
		String coverPath = basePath + videos.getCoverPath();
		File videoFile = new File(videoFilePath);
		File coverFile = new File(coverPath);
		videoFile.delete();
		coverFile.delete();

		videosMapper.deleteByPrimaryKey(id);
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	@Override
	public PagedResult queryVideoList(Integer page, Integer pageSize){
		PageHelper.startPage(page, pageSize);

		Videos example = new Videos();
		List<Videos> list = videosMapper.selectByAllVideo();
		list.forEach(obj -> {
			Users users = usersMapper.selectByPrimaryKey(obj.getUserId());
			obj.setNickname(users.getNickname());
			Bgm bgm = bgmMapper.selectByPrimaryKey(obj.getAudioId());
			if(bgm == null){
				obj.setBgmName("");
			}else{
				obj.setBgmName(bgm.getName());
			}
		});
		PageInfo<Videos> pageList = new PageInfo<>(list);

		PagedResult result = new PagedResult();
		result.setTotal(pageList.getPages());
		result.setRows(list);
		result.setPage(page);
		result.setRecords(pageList.getTotal());

		return result;
	}

}
