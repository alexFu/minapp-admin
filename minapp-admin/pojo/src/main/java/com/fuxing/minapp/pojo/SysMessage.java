package com.fuxing.minapp.pojo;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Table(name = "m_sys_message")
public class SysMessage {
    private String id;

    @Column(name = "sys_user_id")
    private String sysUserId;

    private String message;

    private Date createTime;

    @Transient
    private String sysNickname;

    @Transient
    private String createTimeStr;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(String sysUserId) {
        this.sysUserId = sysUserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getSysNickname() {
        return sysNickname;
    }

    public void setSysNickname(String sysNickname) {
        this.sysNickname = sysNickname;
    }

    public String getCreateTimeStr() {
        return createTimeStr;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }
}
