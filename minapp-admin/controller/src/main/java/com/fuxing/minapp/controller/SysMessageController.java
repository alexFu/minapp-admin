package com.fuxing.minapp.controller;

import com.fuxing.minapp.bean.AdminUser;
import com.fuxing.minapp.pojo.SysMessage;
import com.fuxing.minapp.pojo.SysUser;
import com.fuxing.minapp.service.SysMessageService;
import com.fuxing.minapp.service.SysUserService;
import com.fuxing.minapp.utils.IJSONResult;
import com.fuxing.minapp.utils.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("sysMessage")
public class SysMessageController {

    @Autowired
    private SysMessageService sysMessageService;

    @Autowired
    private SysUserService sysUserService;

    @PostMapping("/pushMessage")
    @ResponseBody
    public IJSONResult pushMessage(SysMessage sysMessage,
                                   HttpServletRequest request, HttpServletResponse response) {
        AdminUser user = (AdminUser) request.getSession().getAttribute("sessionUser");
        SysUser sysUser = sysUserService.selectSysUserByUserName(user.getUsername());
        sysMessage.setSysUserId(sysUser.getSysUserId());
        sysMessageService.pushMessage(sysMessage);
        return IJSONResult.ok();
    }

    @GetMapping("/showSysMessageList")
    public String showSysMessageList(){ return "system/sysMessageList"; }

    @PostMapping("/delSysMessage")
    @ResponseBody
    public IJSONResult delSysMessage(String id){
        sysMessageService.delSysMessage(id);
        return IJSONResult.ok();
    }

    @PostMapping("/querySysMessageList")
    @ResponseBody
    public PagedResult querySysMessageList(Integer page){
        PagedResult result = sysMessageService.querySysMessageList(page, 10);
        return result;
    }
}
