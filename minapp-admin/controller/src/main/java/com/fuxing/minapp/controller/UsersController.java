package com.fuxing.minapp.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.fuxing.minapp.bean.AdminUser;
import com.fuxing.minapp.pojo.SysUser;
import com.fuxing.minapp.pojo.Users;
import com.fuxing.minapp.service.SysUserService;
import com.fuxing.minapp.utils.IJSONResult;
import com.fuxing.minapp.utils.MD5Utils;
import com.fuxing.minapp.utils.PagedResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.fuxing.minapp.service.UsersService;

@Controller
@RequestMapping("users")
public class UsersController {
	
	@Autowired
	private UsersService usersService;

	@Autowired
	private SysUserService sysUserService;
	
	@GetMapping("/showList")
	public String showList() {
		return "users/usersList";
	}
	
	@PostMapping("/list")
	@ResponseBody
	public PagedResult list(Users user , Integer page) {
		
		PagedResult result = usersService.queryUsers(user, page == null ? 1 : page, 10);
		return result;
	}


	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
	@PostMapping("login")
	@ResponseBody
	public IJSONResult userLogin(String username, String password,
								 HttpServletRequest request, HttpServletResponse response) throws Exception {

		if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
			return IJSONResult.errorMap("用户名和密码不能为空");
		} else {
			SysUser sysUser = sysUserService.selectSysUserByUserName(username);
			if(sysUser == null || !MD5Utils.getMD5Str(password).equals(sysUser.getPassword())){
				return IJSONResult.errorMsg("账号或密码错误...");
			}
			String token = UUID.randomUUID().toString();
			AdminUser user = new AdminUser(username, password, token);
			request.getSession().setAttribute("sessionUser", user);

			return IJSONResult.ok();
		}
	}
	
	@GetMapping("/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute("sessionUser");
		return "login";
	}

	@PostMapping("/registerSysUser")
	@ResponseBody
	public IJSONResult registerSysUser(SysUser sysUser){
		SysUser result = sysUserService.selectSysUserByUserName(sysUser.getUsername());
		if(result !=null){
			return IJSONResult.errorMsg("用户已存在...");
		}
		sysUserService.registerSysUser(sysUser);
		return IJSONResult.ok();
	}

	@GetMapping("/addSysUser")
	public String addSysUser(){
		return "users/addSysUser";
	}

	@GetMapping("/modifyPasswordPage")
	public String modifyPasswordPage(){
		return "users/modifyPassword";
	}

	@PostMapping("/modifyPassword")
	@ResponseBody
	public IJSONResult modifyPassword(HttpServletRequest request) throws Exception {
		BufferedReader br=request.getReader();
		String str=br.readLine();
		JSONObject jsonObject = JSONObject.parseObject(str);
		String oldPassword = (String) jsonObject.get("oldPassword");
		String password = (String) jsonObject.get("password");
		AdminUser user = (AdminUser) request.getSession().getAttribute("sessionUser");
		SysUser sysUser = sysUserService.selectSysUserByUserName(user.getUsername());
		if(!MD5Utils.getMD5Str(oldPassword).equals(sysUser.getPassword())){
			return IJSONResult.errorMsg("原密码错误，请重新输入！");
		}
		sysUser.setPassword(password);
		sysUserService.modifyPassword(sysUser);
		return IJSONResult.ok();
	}
	
}
