package com.fuxing.minapp.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.fuxing.minapp.pojo.Bgm;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fuxing.minapp.enums.VideoStatusEnum;
import com.fuxing.minapp.service.VideoService;
import com.fuxing.minapp.utils.IJSONResult;
import com.fuxing.minapp.utils.PagedResult;

@Controller
@RequestMapping("video")
public class VideoController {

    @Autowired
    private VideoService videoService;

    @GetMapping("/showReportList")
    public String showReportList() {
        return "video/reportList";
    }

    @GetMapping("/showAllowPlayVideo")
    public String showAllowPlayVideo() {
        return "video/unReportList";
    }


    @GetMapping("/showVideoList")
    public String showVideoList() {
        return "video/videoList";
    }

    @GetMapping("/pushSysMessage")
    public String pushSysMessage(){ return "system/pushSysMessage"; }


    @PostMapping("/reportList1")
    @ResponseBody
    public PagedResult reportList1(Integer page) {

        PagedResult result = videoService.queryVideoList(page, 10);
        return result;
    }


    @PostMapping("/reportList")
    @ResponseBody
    public PagedResult reportList(Integer page) {

        PagedResult result = videoService.queryReportList(page, 10);
        return result;
    }

    @PostMapping("/unReportList")
    @ResponseBody
    public PagedResult unReportList(Integer page) {

        PagedResult result = videoService.queryUnReportList(page, 10);
        return result;
    }

    @PostMapping("/forbidVideo")
    @ResponseBody
    public IJSONResult forbidVideo(String videoId) {

        videoService.updateVideoStatus(videoId, VideoStatusEnum.FORBID.value);
        return IJSONResult.ok();
    }

    @PostMapping("/allowPlayVideo")
    @ResponseBody
    public IJSONResult allowPlayVideo(String videoId) {

        videoService.updateVideoStatus(videoId, VideoStatusEnum.SUCCESS.value);
        return IJSONResult.ok();
    }

    @GetMapping("/showBgmList")
    public String showBgmList() {
        return "video/bgmList";
    }

    @PostMapping("/queryBgmList")
    @ResponseBody
    public PagedResult queryBgmList(Integer page) {
        return videoService.queryBgmList(page, 10);
    }

    @PostMapping("/queryVideoList")
    @ResponseBody
    public PagedResult queryVideoList(Integer page) {
        return videoService.queryVideoList(page, 10);

    }


    @GetMapping("/showAddBgm")
    public String login() {
        return "video/addBgm";
    }

    @PostMapping("/addBgm")
    @ResponseBody
    public IJSONResult addBgm(Bgm bgm) {
        videoService.addBgm(bgm);
        return IJSONResult.ok();
    }

    @PostMapping("/delBgm")
    @ResponseBody
    public IJSONResult delBgm(String bgmId) {
        videoService.deleteBgm(bgmId);
        return IJSONResult.ok();
    }

    @PostMapping("/delVideo")
    @ResponseBody
    public IJSONResult delVideo(String videoId) {
        videoService.deleteVideo(videoId);
        return IJSONResult.ok();
    }


    @PostMapping("/bgmUpload")
    @ResponseBody
    public IJSONResult bgmUpload(@RequestParam("file") MultipartFile[] files) throws Exception {

        // 文件保存的命名空间
//		String fileSpace = File.separator + "imooc_videos_dev" + File.separator + "mvc-bgm";
        String fileSpace = "D:" + File.separator + "workspace"+ File.separator +"fileUpload";
        // 保存到数据库中的相对路径
        String uploadPathDB = File.separator + "bgm";

        FileOutputStream fileOutputStream = null;
        InputStream inputStream = null;
        try {
            if (files != null && files.length > 0) {

                String fileName = files[0].getOriginalFilename().replace(" ","");
                if (StringUtils.isNotBlank(fileName)) {
                    // 文件上传的最终保存路径
                    String finalPath = fileSpace + uploadPathDB + File.separator + fileName;
                    // 设置数据库保存的路径
                    uploadPathDB += (File.separator + fileName);

                    File outFile = new File(finalPath);
                    if (outFile.getParentFile() != null || !outFile.getParentFile().isDirectory()) {
                        // 创建父文件夹
                        outFile.getParentFile().mkdirs();
                    }

                    fileOutputStream = new FileOutputStream(outFile);
                    inputStream = files[0].getInputStream();
                    IOUtils.copy(inputStream, fileOutputStream);
                }

            } else {
                return IJSONResult.errorMsg("上传出错...");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return IJSONResult.errorMsg("上传出错...");
        } finally {
            if (fileOutputStream != null) {
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        }

        return IJSONResult.ok(uploadPathDB);
    }

}
