var deleteBgm = function(videoId) {
	
	var flag = window.confirm("是否确认删除？？？");
	if (!flag) {
		return;
	}
	
	$.ajax({
		url: $("#hdnContextPath").val() + '/video/delVideo.action?videoId=' + videoId,
		type: "POST",
		success: function(data) {
			if (data.status == 200 && data.msg == 'OK') {
				alert('删除成功~~');
				var jqGrid = $("#videoList");
				jqGrid.jqGrid().trigger("reloadGrid");
			}
		}
	})
}

var BgmList = function() {

	// 构建bgm列表对象
    var handleBgmList = function() {
    	
    	// 上下文对象路径
		var hdnContextPath = $("#hdnContextPath").val();
		var bgmServer = $("#bgmServer").val();
		
		
		var jqGrid = $("#videoList");
        jqGrid.jqGrid({  
            caption: "所有短视频列表",
            url: hdnContextPath + "/video/queryVideoList.action",
            mtype: "post",  
            styleUI: 'Bootstrap',//设置jqgrid的全局样式为bootstrap样式
            datatype: "json",  
            colNames: ['ID', '用户昵称','背景音乐', '视频描述' ,'视频封面', '点赞数', '状态','操作','操作'],
            colModel: [  
                { name: 'id', index: 'id', width: 20 },
                { name: 'nickname', index: 'nickname', width: 20 },
                { name: 'bgmName', index: 'bgmName', width: 20 },
                { name: 'videoDesc', index: 'video_desc', width: 20 },
                { name: 'coverPath', index: 'cover_path', width: 40 ,
                    formatter: function(cellvalue, option, rowObject) {
                        var src ="http://localhost:8081" + cellvalue;
                        var img = "<img src='" + src + "' width='120'></img>"
                        return img;
                    }
                },
                { name: 'likeCounts', index: 'like_counts', width: 20 },
                { name: 'status', index: 'status', width: 20},
                { name: 'videoPath', index: 'video_path', width: 20,
                    formatter: function(cellvalue, option, rowObject){

                        var src = "http://localhost:8081" + cellvalue;
                        var html = "<a href='" + src + "' target='_blank'>点我播放</a>"

                        return html;
                    }
                },
                { name: '', index: '', width: 10,
                	formatter: function(cellvalue, option, rowObject){
                		
                		var html = '<button class="btn btn-outline blue-chambray" id="" onclick=deleteBgm("' + rowObject.id + '") style="padding: 1px 3px 1px 3px;">删除</button>';
                		
                		return html;
                	}
                }
            ],  
            viewrecords: true,  		// 定义是否要显示总记录数
            rowNum: 10,					// 在grid上显示记录条数，这个参数是要被传递到后台
            rownumbers: true,  			// 如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'
            autowidth: true,  			// 如果为ture时，则当表格在首次被创建时会根据父元素比例重新调整表格宽度。如果父元素宽度改变，为了使表格宽度能够自动调整则需要实现函数：setGridWidth
            height: 400,				// 表格高度，可以是数字，像素值或者百分比
            rownumWidth: 36, 			// 如果rownumbers为true，则可以设置行号 的宽度
            pager: "#videoListPager",		// 分页控件的id
            subGrid: false				// 是否启用子表格
        }).navGrid('#videoListPager', {
            edit: false,
            add: false,
            del: false,
            search: false
        });
        
        
        // 随着窗口的变化，设置jqgrid的宽度  
        $(window).bind('resize', function () {  
            var width = $('.videoList_wrapper').width()*0.99;
            jqGrid.setGridWidth(width);  
        });  
        
        // 不显示水平滚动条
        jqGrid.closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
    	
    }
    
    return {
        init: function() {
        	handleBgmList();
        }

    };

}();

jQuery(document).ready(function() {
	BgmList.init();
});