<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
	    <ul class="page-breadcrumb">
	        <li>
	            <span>首页</span>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <span>系统管理</span>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <span>账号注册</span>
	        </li>
	    </ul>
	</div>
	<!-- END PAGE BAR -->
	<!-- END PAGE HEADER-->
                        
	<div class="row">
    	<div class="col-md-12">
			<br/>
			<!-- 意见反馈 start -->
			<div class="tabbable-line boxless tabbable-reversed">
            	<div class="portlet box green-jungle">
                	<div class="portlet-title">
                    	<div class="caption">
                    		<i class="icon-plus"></i>系统账号注册
                    	</div>
					</div>
					
					<div class="portlet-body form">
					
	                    <!-- BEGIN FORM-->
	                    <form id="addSysUserForm" class="form-horizontal">
		                    <div class="form-body">

								<div class="form-group">
									<label class="col-md-3 control-label"><span class="field-required"> * </span>昵称：</label>
									<div class="col-md-4">
										<div id="input-error">
											<input id="nickname" name="nickname" type="text" class="form-control" placeholder="昵称">
										</div>
									</div>
								</div>

		                    	<div class="form-group">
		                        	<label class="col-md-3 control-label"><span class="field-required"> * </span>用户名：</label>
		                            <div class="col-md-4">
		                            	<div id="input-error">
		                            		<input id="username" name="username" type="text" class="form-control" placeholder="用户名">
		                            	</div>
									</div>
								</div>
								
								<div class="form-group">
		                        	<label class="col-md-3 control-label"><span class="field-required"> * </span>密码：</label>
		                            <div class="col-md-4">
		                            	<div id="input-error">
		                            		<input id="password" name="password" type="password" class="form-control" placeholder="密码">
		                            	</div>
									</div>
								</div>
								
								<div class="form-group">
		                        	<label class="col-md-3 control-label"><span class="field-required"> * </span>确认密码</label>
									<div class="col-md-4">
										<div id="input-error">
											<input id="passwordRep" name="passwordRep" type="password" class="form-control" placeholder="确认密码">
										</div>
									</div>
								</div>
		                                                  
							</div>
	                                                        
							<div class="form-actions">
			                    <div class="row">
			                        <div class="col-md-offset-3 col-md-9">
			                            <button type="submit" class="btn green-jungle">提 交</button>
			                            <button type="reset" class="btn grey-salsa btn-outline">取  消</button>
			                        </div>
			                    </div>
							</div>
						</form>
						<!-- END FORM-->
						
					</div>
				</div>
			</div>
                            
		</div>
	</div>
	
<script type="text/javascript">

    var submitSysUser = function() {
    	var password = $("#password").val();
    	var passwordRep = $("#passwordRep").val();
    	if(password != passwordRep){
    		alert("两次密码不一致，请确认后再提交");
    		return;
		}
    	$('#addSysUserForm').ajaxSubmit({
    		url: $('#hdnContextPath').val() + '/users/registerSysUser.action',
    		type: 'POST',
    		success: function(data) {
    			console.log(data);
    			if (data.status == 200 && data.msg == 'OK') {
    				alert('注册成功...');
    			} else {
    				alert(data.msg);
    			}
				$("#indexPageMenu").click();
    		}
    	});
    }
    
    $('#addSysUserForm').validate({
    	errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
			nickname: {
				required: true
			},
			username: {
                required: true,
                rangelength: [4,16]
            },
            password: {
                required: true,
                rangelength: [4,20]
            },
			passwordRep: {
                required: true,
				rangelength: [4,20]
            }
        },
        messages: {
			nickname: {
				required: "昵称不能为空."
			}
			username: {
                required: "用户名不能为空.",
                rangelength: "用户长度请控制在4-16位."
            },
            password: {
                required: "密码不能为空.",
                rangelength: "密码长度请控制在4-20位."
            },
            passwordRep: {
                required: "请再次确认密码.",
				rangelength: "密码长度请控制在4-20位."
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit   
            $('.alert-danger', $('#addSysUserForm')).show();
        },

        highlight: function(element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.closest('#input-error'));
        },
        submitHandler: function(form) {
        	// FIXME
			submitSysUser();
        }
    });
    
    
</script>
