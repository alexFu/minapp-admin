<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
	    <ul class="page-breadcrumb">
	        <li>
	            <span>首页</span>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <span>系统管理</span>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <span>密码修改</span>
	        </li>
	    </ul>
	</div>
	<!-- END PAGE BAR -->
	<!-- END PAGE HEADER-->
                        
	<div class="row">
    	<div class="col-md-12">
			<br/>
			<!-- 意见反馈 start -->
			<div class="tabbable-line boxless tabbable-reversed">
            	<div class="portlet box green-jungle">
                	<div class="portlet-title">
                    	<div class="caption">
                    		<i class="icon-plus"></i>修改个人密码
                    	</div>
					</div>
					
					<div class="portlet-body form">
					
	                    <!-- BEGIN FORM-->
	                    <form id="modifyPasswordForm" class="form-horizontal">
		                    <div class="form-body">

								<div class="form-group">
									<label class="col-md-3 control-label"><span class="field-required"> * </span>原密码：</label>
									<div class="col-md-4">
										<div id="input-error">
											<input id="oldPassword" name="nickname" type="password" class="form-control" placeholder="原密码">
										</div>
									</div>
								</div>

		                    	<div class="form-group">
		                        	<label class="col-md-3 control-label"><span class="field-required"> * </span>新密码：</label>
		                            <div class="col-md-4">
		                            	<div id="input-error">
		                            		<input id="password" name="username" type="password" class="form-control" placeholder="新密码">
		                            	</div>
									</div>
								</div>
								
								<div class="form-group">
		                        	<label class="col-md-3 control-label"><span class="field-required"> * </span>确认密码：</label>
		                            <div class="col-md-4">
		                            	<div id="input-error">
		                            		<input id="passwordRep" name="password" type="password" class="form-control" placeholder="确认密码">
		                            	</div>
									</div>
								</div>

		                                                  
							</div>
	                                                        
							<div class="form-actions">
			                    <div class="row">
			                        <div class="col-md-offset-3 col-md-9">
			                            <button type="submit" class="btn green-jungle">提 交</button>
			                            <button type="reset" class="btn grey-salsa btn-outline">取  消</button>
			                        </div>
			                    </div>
							</div>
						</form>
						<!-- END FORM-->
						
					</div>
				</div>
			</div>
                            
		</div>
	</div>
	
<script type="text/javascript">

	var submitModifyPassword = function() {
		var oldPassword = $("#oldPassword").val();
		var password = $("#password").val();
		var passwordRep = $("#passwordRep").val();
		if(password != passwordRep){
			alert("两次密码不一致，请确认后再提交");
			return;
		}
		if(oldPassword == password){
			alert("新密码和原密码一致，无法修改");
			return;
		}
		var obj = {
			oldPassword: oldPassword,
			password: password
		};
		var data = JSON.stringify(obj);
		$.ajax({
			url: $("#hdnContextPath").val() + '/users/modifyPassword.action',
			type: "POST",
			data: data,
			contentType : 'application/json;charset=UTF-8',
			success: function(data) {
				if (data.status == 200 && data.msg == 'OK') {
					alert('修改成功...');
					$.ajax({
						url: $("#hdnContextPath").val() + '/users/logout.action',
						type: "GET",
						success: function(data) {
							window.location.href='http://localhost:8080/imooc_video_admin_mng_war_exploded/users/login.action'
						}
					})
				} else {
					alert(data.msg);
				}
			}
		})
	}
    
    $('#modifyPasswordForm').validate({
    	errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
			oldPassword: {
                required: true,
                rangelength: [4,20]
            },
            password: {
                required: true,
                rangelength: [4,20]
            },
			passwordRep: {
                required: true,
				rangelength: [4,20]
            }
        },
        messages: {
			username: {
                required: "原密码不能为空.",
                rangelength: "用户长度请控制在4-20位."
            },
            password: {
                required: "密码不能为空.",
                rangelength: "密码长度请控制在4-20位."
            },
            passwordRep: {
                required: "请再次确认密码.",
				rangelength: "密码长度请控制在4-20位."
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit   
            $('.alert-danger', $('#modifyPasswordForm')).show();
        },

        highlight: function(element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.closest('#input-error'));
        },
        submitHandler: function(form) {
        	// FIXME
			submitModifyPassword();
        }
    });
    
    
</script>
