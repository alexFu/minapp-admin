<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="<%=request.getContextPath() %>/static/pages/js/sysMessageList.js?v=1.0.0.2"
	type="text/javascript"></script>

	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
	    <ul class="page-breadcrumb">
	        <li>
	            <span>首页</span>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <span>系统管理</span>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <span>系统消息管理</span>
	        </li>
	    </ul>
	</div>
	<!-- END PAGE BAR -->
	<!-- END PAGE HEADER-->
                        
	<div class="row">
    	<div class="col-md-12">
                   
			<div class="sysMessageList_wrapper">
                <table id="sysMessageList"></table>
    			<div id="sysMessageListPager"></div>
             </div>
             
		</div>
	</div>
