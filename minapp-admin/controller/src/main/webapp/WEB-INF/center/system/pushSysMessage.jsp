<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
	    <ul class="page-breadcrumb">
	        <li>
	            <span>首页</span>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <span>系统管理</span>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <span>推送消息</span>
	        </li>
	    </ul>
	</div>
	<!-- END PAGE BAR -->
	<!-- END PAGE HEADER-->
                        
	<div class="row">
    	<div class="col-md-12">
			<br/>
			<!-- 意见反馈 start -->
			<div class="tabbable-line boxless tabbable-reversed">
            	<div class="portlet box green-jungle">
                	<div class="portlet-title">
                    	<div class="caption">
                    		<i class="icon-plus"></i>发送系统消息
                    	</div>
					</div>
					
					<div class="portlet-body form">
					
	                    <!-- BEGIN FORM-->
	                    <form id="addBgmForm" class="form-horizontal">
		                    <div class="form-body">
		                    
		                    	<div class="form-group">
		                        	<label class="col-md-3 control-label"><span class="field-required"> * </span>消息：</label>
		                            <div class="col-md-4">
		                            	<div id="input-error">
		                            		<textarea id="message" name="message" maxlength="200" class="form-control" style="min-height: 130px;" placeholder="200字以内"></textarea>
		                            	</div>
									</div>
								</div>
		                                                  
							</div>
	                                                        
							<div class="form-actions">
			                    <div class="row">
			                        <div class="col-md-offset-3 col-md-9">
			                            <button type="submit" class="btn green-jungle" onclick="pushMessage()">提 交</button>
			                            <button type="reset" class="btn grey-salsa btn-outline">取  消</button>
			                        </div>
			                    </div>
							</div>
						</form>
						<!-- END FORM-->
						
					</div>
				</div>
			</div>
                            
		</div>
	</div>
	
<script type="text/javascript">

	function pushMessage(){
		var message = $("#message").val();
		$.ajax({
			url : 'sysMessage/pushMessage.action',
			type : "post",
			dataType : "json",
			data : {
				message: message
			},
			cache : false,
			async : false,
			success : function(data) {
				if (data.status == 200 && data.msg == 'OK'){
					alert('消息推送成功...')
				}else{
					alert("消息推送失败...")
				}
			}
		});
	}
    
</script>
