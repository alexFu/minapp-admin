<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script src="<%=request.getContextPath() %>/static/pages/js/unReportList.js?v=1.1" type="text/javascript"></script>

	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
	    <ul class="page-breadcrumb">
	        <li>
	            <span>首页</span>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <span>短视频管理</span>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <span>解禁短视频</span>
	        </li>
	    </ul>
	</div>
	<!-- END PAGE BAR -->
	<!-- END PAGE HEADER-->
        
    <!-- 列表 jqgrid start -->                
	<div class="row">
	
    	<div class="col-md-12">
			<br/>
			
			<div class="unUsersReportsList_wrapper">
			    <table id="unUsersReportsList"></table>
			    <div id="unUsersReportsListPager"></div>
			</div>  
			
		</div>
	</div>
	<!-- 列表 jqgrid end -->
	
