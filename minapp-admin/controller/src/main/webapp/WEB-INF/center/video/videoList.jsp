<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="<%=request.getContextPath() %>/static/pages/js/videoList.js?v=1.0.0.2"
	type="text/javascript"></script>

	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
	    <ul class="page-breadcrumb">
	        <li>
	            <span>首页</span>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <span>短视频管理</span>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <span>短视频列表展示</span>
	        </li>
	    </ul>
	</div>
	<!-- END PAGE BAR -->
	<!-- END PAGE HEADER-->
                        
	<div class="row">
    	<div class="col-md-12">
                   
			<div class="videoList_wrapper">
                <table id="videoList"></table>
    			<div id="videoListPager"></div>
             </div>
             
		</div>
	</div>
