package com.fuxing.minapp.mapper;

import com.fuxing.minapp.pojo.Bgm;
import com.fuxing.minapp.pojo.BgmExample;
import com.fuxing.minapp.pojo.SysMessage;
import com.fuxing.minapp.pojo.Videos;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysMessageMapper {

    void saveMessage(SysMessage sysMessage);

    List<SysMessage> showSysMessage();

    void deleteByPrimaryKey(@Param("id") String id);

}