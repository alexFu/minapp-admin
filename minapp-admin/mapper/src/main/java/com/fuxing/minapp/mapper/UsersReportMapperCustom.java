package com.fuxing.minapp.mapper;

import java.util.List;

import com.fuxing.minapp.pojo.vo.Reports;

public interface UsersReportMapperCustom {
    List<Reports> selectAllVideoReport();

    List<Reports> selectAllForbidReport();
}