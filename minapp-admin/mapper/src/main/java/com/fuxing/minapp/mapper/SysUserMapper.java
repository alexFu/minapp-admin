package com.fuxing.minapp.mapper;

import com.fuxing.minapp.pojo.SysMessage;
import com.fuxing.minapp.pojo.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysUserMapper {


    SysUser selectSysUserByUserName(@Param("username") String username);

    void registerSysUser(SysUser sysUser);

    SysUser selectByPrimaryKey(@Param("sysUserId") String sysUserId);

    void updatePassword(SysUser sysUser);
}